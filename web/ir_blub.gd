extends Node2D

export (String) var server = "192.168.1.159"
export(String, "blue", "red", "white") var  light_setting
var code = ""

onready var http_request = HTTPRequest.new()

func _ready():
	add_child(http_request)
	http_request.connect("request_completed", self, "_http_request_completed")
	
	
func set_light():

	if light_setting == "blue":
		code = "16756815";
	elif light_setting == "red":
		code = "16724175";
	elif light_setting == "white":
		code = "16732845";
	
	var url = "http://"+server+"/ir?code="+code;
	#print(url)
	http_request.request(url)
	
func _http_request_completed(result, response_code, headers, body):
	#print(body.get_string_from_utf8())
	pass


func _on_Area2D_body_entered(body):
	if body.is_in_group("players"):
		set_light()
