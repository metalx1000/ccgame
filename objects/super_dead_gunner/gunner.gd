extends KinematicBody2D


export (bool) var active = false
export (int) var speed = 1000
export (int) var gravity = 1000
export (int) var direction = 1
export (bool) var can_fall = false


var velocity = Vector2.ZERO

var explosion = load("res://objects/explosion/explosion.tscn")
#var explosion = load("res://objects/explosion/explosion_2.tscn")
var explode = explosion.instance()

onready var sprite = $sprite
onready var ray = $Area2D/RayCast2D
onready var ground_check_r = $Area2D/ground_check_r
onready var ground_check_l = $Area2D/ground_check_l
onready var offscreen_timer =$offscreen_timer

var mover = Vector2.ZERO

# Called when the node enters the scene tree for the first time.
#func _ready():
	



# Called every frame. 'delta' is the elapsed time since the previous frame.
func  _physics_process(delta):
	velocity.x = 0
	if active:
		walk(delta)

		velocity.y += gravity * delta
		velocity = move_and_slide(velocity, Vector2.UP)
	
func walk(delta):
	if ray.is_colliding():
		direction *= -1
		
	if !can_fall:
		if !ground_check_l.is_colliding() || !ground_check_r.is_colliding():
			direction *= -1
		
	ray.global_rotation_degrees = rad2deg(mover.angle()) - 90 * (direction * -1)
	if direction == 1:
		sprite.flip_h = false
		sprite.play("walk") 
		
	
	if direction == -1:
		sprite.flip_h = true
		sprite.play("walk") 
		
	velocity.x -= speed * delta * 2 * direction
	


func _on_VisibilityNotifier2D_viewport_entered(viewport):
	offscreen_timer.stop()
	active = true


func _on_VisibilityNotifier2D_viewport_exited(viewport):
	offscreen_timer.start()


func _on_offscreen_timer_timeout():
	#if gunner us off screen for 5 seconds then set to inactive
	active = false

func death():
	get_tree().get_current_scene().add_child(explode)
	explode.position = position
	queue_free()


func _on_Area2D_area_shape_entered(area_id, area, area_shape, self_shape):
	if area.is_in_group("explosion"):
		death()
