extends KinematicBody2D

export (bool) var active = true
export (int) var gravity = 1000
export (int) var health = 100

var velocity = Vector2.ZERO

#var explosion = load("res://objects/explosion/explosion.tscn")
var explosion = load("res://objects/explosion/explosion_2.tscn")
var explode = explosion.instance()


onready var sprite = $sprite
onready var offscreen_timer =$offscreen_timer

var mover = Vector2.ZERO

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("default")
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func  _physics_process(delta):
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)
	

func _on_VisibilityNotifier2D_viewport_entered(viewport):
	offscreen_timer.stop()
	active = true


func _on_VisibilityNotifier2D_viewport_exited(viewport):
	offscreen_timer.start()


func _on_offscreen_timer_timeout():
	#if gunner us off screen for 5 seconds then set to inactive
	active = false

func death():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var r = rng.randf_range(0, .5)
	yield(get_tree().create_timer(r), "timeout")
	$AnimationPlayer.play("death")
	add_child(explode)
	explode.position.y = 10


func _on_Area2D_body_entered(body):
	
	if body.is_in_group("players"):
			death()


func _on_Area2D_area_shape_entered(area_id, area, area_shape, self_shape):
	if area.is_in_group("explosion"):
		death()
