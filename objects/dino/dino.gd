extends KinematicBody2D

export (int) var speed = 3200
export (int) var jump_speed = -400
export (int) var gravity = 1000

var score = 0
var velocity = Vector2.ZERO

onready var sprite=$sprite

onready var http_request = HTTPRequest.new()
var url = "https://filmsbykris.com/scripts/php/godot-log/write.php?score="

func _ready():
	add_child(http_request)
	http_request.connect("request_completed", self, "_http_request_completed")
	set_color(.8)
	set_score()
	http_request.request(url + str(score))
	
func _physics_process(delta):
	get_input(delta)
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)

func set_score(value:int = 0):
	score += value
	print(score);
	
func set_color(v):
	# Duplicate the shader so that changing its param doesn't change it on any other sprites that also use the shader.
	# Generally done once in _ready()
	sprite.set_material(sprite.get_material().duplicate(true))

	# Offset sprite hue by a random value within specified limits.
	#var rand_hue = float(randi() % 3)/2.0/3.2
	sprite.material.set_shader_param("Shift_Hue", v)

func get_input(delta):
	velocity.x = 0
	
	if Input.is_action_pressed("ui_right"):
		sprite.flip_h = false
		sprite.play("walk")
		velocity.x += speed * delta * 2
	if Input.is_action_pressed("ui_left"):
		sprite.flip_h = true
		sprite.play("walk") 
		velocity.x -= speed * delta * 2
		
	if velocity.x < 30 && velocity.x > -30:
		sprite.play("default")
		
	if Input.is_action_just_pressed("ui_up"):
		if is_on_floor():
			$jump_snd.play()
			velocity.y = jump_speed
			
func _http_request_completed(result, response_code, headers, body):
	http_request.request(url + str(score))
	pass
