extends KinematicBody2D

export (int) var speed = 3200
export (int) var jump_speed = -400
export (int) var gravity = 1000

var velocity = Vector2.ZERO

onready var sprite=$sprite

func get_input(delta):
	velocity.x = 0
	
	if Input.is_action_pressed("ui_right"):
		sprite.flip_h = false
		sprite.play("walk")
		velocity.x += speed * delta * 2
	if Input.is_action_pressed("ui_left"):
		sprite.flip_h = true
		sprite.play("walk") 
		velocity.x -= speed * delta * 2
		
	if velocity.x < 30 && velocity.x > -30:
		sprite.play("default")

func _physics_process(delta):
	get_input(delta)
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)
	if Input.is_action_just_pressed("ui_up"):
		if is_on_floor():
			velocity.y = jump_speed
