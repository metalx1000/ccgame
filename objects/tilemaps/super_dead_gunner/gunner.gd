extends KinematicBody2D


export (bool) var active = false
export (int) var speed = 1000
export (int) var gravity = 1000
export (int) var direction = 1

var velocity = Vector2.ZERO

onready var sprite = $sprite
onready var ray = $Area2D/RayCast2D
var mover = Vector2.ZERO

# Called when the node enters the scene tree for the first time.
#func _ready():
	



# Called every frame. 'delta' is the elapsed time since the previous frame.
func  _physics_process(delta):
	velocity.x = 0
	if active:
		walk(delta)

	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)
	
func walk(delta):
	if ray.is_colliding():
		direction *= -1
		
	ray.global_rotation_degrees = rad2deg(mover.angle()) - 90 * (direction * -1)
	if direction == 1:
		sprite.flip_h = false
		sprite.play("walk") 
		
	
	if direction == -1:
		sprite.flip_h = true
		sprite.play("walk") 
		
	velocity.x -= speed * delta * 2 * direction
	
